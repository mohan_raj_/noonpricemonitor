package pricechecker

func productExistsInDB(p Product) bool {
	return p.URL != ""
}

func isProductCheaperThanBefore(currentProduct, productFromDB Product) bool {
	if salePriceNotAvailable(currentProduct) {
		return isCurrentPriceCheaper(currentProduct, productFromDB)
	}

	return isCurrentSalePriceCheaper(currentProduct, productFromDB)
}

func isCurrentPriceCheaper(currentProductInfo, previousProductInfo Product) bool {
	if salePriceNotAvailable(previousProductInfo) {
		return currentProductInfo.Price < previousProductInfo.Price
	}

	return currentProductInfo.Price < previousProductInfo.SalePrice
}

func isCurrentSalePriceCheaper(currentProductInfo, previousProductInfo Product) bool {
	if salePriceNotAvailable(previousProductInfo) {
		return currentProductInfo.SalePrice < previousProductInfo.Price
	}

	return currentProductInfo.SalePrice < previousProductInfo.SalePrice
}

func salePriceNotAvailable(product Product) bool {
	return product.SalePrice == 0
}

// Let's see what we need to do in our main()
// Go to the URL and get all the products
// For each product, check if the product is already in the DB
// If not, add the product to the DB
// If it is already in the DB, compare previous price to current price.
// If the current price is less than prev. price by 10% or more, notify

// So, it boils down to this:
// func main() {
// discountedProducts := make([]Product, 0, 10)
// products := GetAllProducts()
//     for _, p := range products {
//         productFromDB := GetProductByURL(p.URL)
//         if (productExistsInDB(productFromDB)) {
//             if isProductCheaperThanBefore(p, productFromDB) {
//                 discountedProducts = append(discountedProducts, p)
//             }
//         } else {
//             AddProduct(p)
//         }
//     }
//
//     alertForProducts(discountedProducts)
// }
