package pricechecker

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DBInfo is a struct that contains the connection details of the DB
// and table the data is stored in
type DBInfo struct {
	URL            string
	DBName         string
	CollectionName string
}

type mongoClientInfo struct {
	client     *mongo.Client
	ctx        context.Context
	cancelFunc context.CancelFunc
}

// ConnectionString is where you specify the connection string for the DB
const ConnectionString string = "mongodb://localhost:27017"

// GetProductByURL returns a product from the DB filtered by url
func GetProductByURL(dbInfo DBInfo, url string) (Product, error) {
	clientInfo, err := connectToDB(dbInfo)
	if err != nil {
		return Product{}, err
	}
	defer clientInfo.client.Disconnect(clientInfo.ctx)
	defer clientInfo.cancelFunc()

	filter := bson.M{
		"url": url,
	}
	product := Product{}

	collection := clientInfo.client.Database(dbInfo.DBName).Collection(dbInfo.CollectionName)
	err = collection.FindOne(clientInfo.ctx, filter).Decode(&product)
	if err != nil {
		return Product{}, err
	}

	return product, nil
}

// AddProduct adds a product into the DB
func AddProduct(dbInfo DBInfo, product Product) error {
	clientInfo, err := connectToDB(dbInfo)
	if err != nil {
		return err
	}
	defer clientInfo.client.Disconnect(clientInfo.ctx)
	defer clientInfo.cancelFunc()

	collection := clientInfo.client.Database(dbInfo.DBName).Collection(dbInfo.CollectionName)
	_, err = collection.InsertOne(clientInfo.ctx, product)

	return err
}

func connectToDB(dbInfo DBInfo) (mongoClientInfo, error) {
	// Creating a client
	clientInfo, err := createDBClient(dbInfo.URL)
	if err != nil {
		clientInfo.cancelFunc()
		return clientInfo, err
	}

	// Connecting to DB
	err = clientInfo.client.Connect(clientInfo.ctx)
	if err != nil {
		clientInfo.cancelFunc()
		return clientInfo, err
	}
	fmt.Println("Connected to DB")

	return clientInfo, nil
}

func createDBClient(connectionString string) (mongoClientInfo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI(connectionString))

	return mongoClientInfo{
		client:     client,
		ctx:        ctx,
		cancelFunc: cancel,
	}, err
}
