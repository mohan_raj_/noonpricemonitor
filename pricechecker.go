package pricechecker

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/html"
)

// region Declarations

const (
	urlConfigPath = "urlToMonitor.json"
)

// urlContainer  is created to hold all the  url read from json file when unmarshaled
type urlContainer struct {
	// URLs  holds a list of urls that needs to be monitored
	URL []string `json:"url_list"`
}

var urlList urlContainer

// Product is a type that holds all the details of a Product found in each page
type Product struct {
	// OfferCode and Sku details
	OfferCode string `json:"offer_code"`
	Sku       string `json:"sku"`
	SkuConfig string `json:"sku_config"`

	// Brand name
	Brand string `json:"brand"`

	// Name of the product
	Name string `json:"name"`

	// Price before discount or offer
	Price int `json:"price"`

	// SalePrice after discounts
	SalePrice int `json:"sale_price"`

	// URL for the product page
	URL       string   `json:"url"`
	ImageKey  string   `json:"image_key"`
	IsBuyable bool     `json:"is_buyable"`
	Flags     []string `json:"flags"`
}

// endregion

func main() {
	loopThroughTheURLs()
}

// Run through URLs present in the file and update the products
func loopThroughTheURLs() error {
	urlsJSON, err := getJSONFromConfigFile()
	if err != nil {
		fmt.Println(err)
	}

	json.Unmarshal(urlsJSON, &urlList)
	fmt.Println(urlList)
	for _, url := range urlList.URL {
		fmt.Println("Processing the process of the processing url Link -", url)
		loadTheURLUpdateToDB(url)
	}

	return nil
}

// getJSONFromConfigFile gives a []byte that contains the json data of URLs
func getJSONFromConfigFile() ([]byte, error) {
	fileReader, err := getFileInReadMode(urlConfigPath)
	if err != nil {
		return nil, err
	}
	return readContentFromReader(fileReader)
}

// getFileInReadMode requires a path to the file that needs to be opened.
// It returns a pointer to File and error if any.
func getFileInReadMode(filePath string) (*os.File, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	return file, nil
}

// readContentFromReader requires a pointer to file that needs to be read.
// It returns a []byte of read content and error if any.
func readContentFromReader(reader io.Reader) ([]byte, error) {
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// invokewebRequest hits an URL and gets a response pointer [*http.Response].
// It returns an error if any
func invokeWebRequest(url string) (*http.Response, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// getResponse gives the response body of an url provide in []bytes.
// It returns an error if any
func getResponse(url string) (*html.Node, error) {
	responseReader, err := invokeWebRequest(url)
	if err != nil {
		return nil, err
	}

	responseBodyAsString, err := readContentFromReader(responseReader.Body)
	if err != nil {
		return nil, err
	}

	htmlDoc, err := html.Parse(strings.NewReader(string(responseBodyAsString)))
	if err != nil {
		return nil, err
	}

	return htmlDoc, nil

}

// loadTheURLUpdateToDB
func loadTheURLUpdateToDB(url string) {
	pageNumber := 2
	firstPage := url // Appending of "?page=1" is not required for first page
	iterateOnPagesAndUpdateProducts(firstPage)
	for {
		nextPage := fmt.Sprintf("%v?page=%d", url, pageNumber)
		if !(iterateOnPagesAndUpdateProducts(nextPage)) {
			break
		}
		pageNumber++
	}
}

// iterateOnPagesAndUpdateProducts
func iterateOnPagesAndUpdateProducts(url string) bool {
	fmt.Println("--", url, "--")
	body, err := getResponse(url)
	if err != nil {
		log.Fatal(err)
	}

	if !(validatePageExists(url, body)) {
		fmt.Println("the url is not found in the loaded page")
		return false
	}

	updateTheProducts(body)

	return true
}

// validatePageExists with the page number provided.
// If the  page number is not matching with the URL provided then it is understood that the default page
// has loaded (previous page / last page). So we ignore the pageloaded (response body received).
func validatePageExists(url string, htmlDoc *html.Node) bool {
	linkNode, _ := getTheRequiredNode(htmlDoc, "link", "canonical")
	fmt.Println("The node for verifying the url loaded -", linkNode)
	for i := range linkNode.Attr {
		if linkNode.Attr[i].Val == url {
			fmt.Println("The url is found")
			return true
		}
	}
	return false
}

func getTheRequiredNode(htmlDoc *html.Node, nodeName string, firstAttrValue string) (*html.Node, error) {
	var body *html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == nodeName {
			if len(node.Attr) > 0 && node.Attr[0].Val == firstAttrValue {
				body = node
				return
			}
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(htmlDoc)
	if body != nil {
		return body, nil
	}
	return nil, errors.New("Missing the required node in the node tree")
}

func renderNodeToString(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, n)
	return buf.String()
}

func parseBodyToProductsJSON(htmlDoc *html.Node) string {
	productNode, _ := getTheRequiredNode(htmlDoc, "script", "__NEXT_DATA__")
	nodeAsString := renderNodeToString(productNode)

	nodeAsString = strings.TrimLeft(nodeAsString, `<script id="__NEXT_DATA__" type="application/json">`)
	nodeAsString = strings.TrimRight(nodeAsString, `</script>`)

	return nodeAsString
}

func updateTheProducts(htmlDoc *html.Node) {
	// parse the response body and fetch the products

	jsonContent := parseBodyToProductsJSON(htmlDoc)
	jsonSchema := struct {
		Props struct {
			PageProps struct {
				Catalog struct {
					Products []Product `json:"hits"`
				} `json:"catalog"`
			} `json:"pageProps"`
		} `json:"props"`
	}{}

	json.Unmarshal([]byte(jsonContent), &jsonSchema)

	updateTheProductsToDB(jsonSchema.Props.PageProps.Catalog.Products)
	//fmt.Println(jsonContent)
}

func updateTheProductsToDB(productList []Product) {
	for _, v := range productList {
		fmt.Println(v.Name)
	}
}
