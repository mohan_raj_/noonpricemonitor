package pricechecker

import (
	"fmt"
	"testing"
)

var dbInfo = DBInfo{
	URL:            "mongodb://localhost:27017",
	DBName:         "noon",
	CollectionName: "products",
}

func TestGetProduct(t *testing.T) {
	product, err := GetProductByURL(dbInfo, "airpods-2019-with-charging-case-white")
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}

	fmt.Printf("%+v\n", product)

	if product.Name != "AirPods (2019) With Charging Case White" {
		t.Fail()
	}
}

func TestAddProduct(t *testing.T) {
	product := Product{
		OfferCode: "",
		Sku:       "",
		SkuConfig: "",
		SalePrice: 10,
		Price:     15,
		Name:      "UFC Coconut water",
		URL:       "ufc-coconut-water",
		Brand:     "UFC",
		ImageKey:  "",
		IsBuyable: true,
		Flags:     []string{},
	}

	err := AddProduct(dbInfo, product)
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}
}
